/* Copyright 2020, Salvador Carballo
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 
/** @brief This is a simple GPIO and UART example.
 */

/** \addtogroup gpio-uart GPIO and UART example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "main.h"
#include "ciaaUART.h"
#include "board.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);


/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000);

	ciaaUARTInit();
}


/*==================[external functions definition]==========================*/

void SysTick_Handler(void)
{

}

int main(void)
{
	uint8_t rx_buf[5];
	uint8_t rx_bytes;

	initHardware();

	while (1)
	{
		//Control del LED rojo usando la UART 2
		rx_bytes = uartRecv(CIAA_UART_USB, rx_buf, sizeof(rx_buf));

		if(rx_bytes > 0){
			if(rx_buf[0] == '1') {
				Board_LED_Set(0, true);
				uartSend(CIAA_UART_USB, "LED ON!\n", 8);
			}
			if(rx_buf[0] == '0') {
				Board_LED_Set(0, false);
				uartSend(CIAA_UART_USB, "LED OFF!\n", 9);
						}
		}

		//Control del LED rojo usando los botones
		if(Buttons_GetStatus() == TEC1_PRESSED) {
			Board_LED_Set(0, true);
		}
		if(Buttons_GetStatus() == TEC2_PRESSED) {
					Board_LED_Set(0, false);
				}
		__WFI();
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
